## Задача 1: Обеспечить разработку

Предложите решение для обеспечения процесса разработки: хранение исходного кода, непрерывная интеграция и непрерывная поставка. 
Решение может состоять из одного или нескольких программных продуктов и должно описывать способы и принципы их взаимодействия.

Решение должно соответствовать следующим требованиям:
- облачная система;
- система контроля версий Git;
- репозиторий на каждый сервис;
- запуск сборки по событию из системы контроля версий;
- запуск сборки по кнопке с указанием параметров;
- возможность привязать настройки к каждой сборке;
- возможность создания шаблонов для различных конфигураций сборок;
- возможность безопасного хранения секретных данных (пароли, ключи доступа);
- несколько конфигураций для сборки из одного репозитория;
- кастомные шаги при сборке;
- собственные докер-образы для сборки проектов;
- возможность развернуть агентов сборки на собственных серверах;
- возможность параллельного запуска нескольких сборок;
- возможность параллельного запуска тестов.

Обоснуйте свой выбор.

## Ответ:

### Решение для обеспечения процесса разработки

Для обеспечения процесса разработки, включая хранение исходного кода, непрерывную интеграцию и непрерывную поставку в облачной среде с использованием системы контроля версий Git, предлагается использовать следующий набор инструментов:

### GitLab CI/CD:

- **Хранение исходного кода**: GitLab предоставляет встроенную систему контроля версий Git и возможность создания репозиториев для каждого сервиса.

- **Непрерывная интеграция и поставка**: GitLab CI/CD позволяет запускать сборки автоматически по событию из системы контроля версий или по кнопке с указанием параметров. Можно также настроить конфигурации сборок и привязать настройки к каждой сборке.

- **Шаблоны для различных конфигураций сборок**: GitLab позволяет создавать шаблоны CI/CD для удобного использования различных конфигураций сборок.

- **Хранение секретных данных**: GitLab предоставляет возможность безопасного хранения секретных данных с помощью встроенной системы управления переменными.

- **Кастомные шаги при сборке**: GitLab CI/CD поддерживает настройку кастомных шагов при сборке, что позволяет настраивать процесс сборки под конкретные требования проекта.

- **Использование собственных докер-образов для сборки проектов**: GitLab CI/CD позволяет определять собственные докер-образы для использования при сборке проектов.

- **Развертывание агентов сборки на собственных серверах**: GitLab предоставляет возможность развертывания собственных агентов сборки на собственных серверах для дополнительного контроля и оптимизации ресурсов.

- **Параллельный запуск нескольких сборок и тестов**: GitLab CI/CD поддерживает параллельный запуск нескольких сборок и тестов, что позволяет ускорить процесс разработки.

### GitLab Registry:

- **Хранение Docker-образов**: GitLab Registry может использоваться для хранения собственных докер-образов, созданных для сборки проектов.

Выбор GitLab в данном случае обоснован его широкими возможностями в области непрерывной интеграции и поставки, интеграции с системой контроля версий Git, а также возможностью развертывания на собственных серверах для обеспечения дополнительной гибкости и контроля.


## Задача 2: Логи

Предложите решение для обеспечения сбора и анализа логов сервисов в микросервисной архитектуре.
Решение может состоять из одного или нескольких программных продуктов и должно описывать способы и принципы их взаимодействия.

Решение должно соответствовать следующим требованиям:
- сбор логов в центральное хранилище со всех хостов, обслуживающих систему;
- минимальные требования к приложениям, сбор логов из stdout;
- гарантированная доставка логов до центрального хранилища;
- обеспечение поиска и фильтрации по записям логов;
- обеспечение пользовательского интерфейса с возможностью предоставления доступа разработчикам для поиска по записям логов;
- возможность дать ссылку на сохранённый поиск по записям логов.

Обоснуйте свой выбор.

## Ответ:
### Решение для сбора и анализа логов в микросервисной архитектуре

Для обеспечения сбора и анализа логов сервисов в микросервисной архитектуре предлагается использовать следующее решение:

### Elastic Stack (ранее ELK Stack):

- **Сбор логов в центральное хранилище**: В состав Elastic Stack входят такие компоненты как Elasticsearch, Logstash и Kibana. Logstash может использоваться для сбора логов со всех хостов, обслуживающих систему, и их отправки в Elasticsearch для хранения и анализа.

- **Минимальные требования к приложениям**: Для сбора логов из stdout приложений, можно использовать стандартные возможности логирования приложений, например, с помощью библиотеки Logback для Java приложений или stdout/stderr для приложений, работающих в контейнерах.

- **Гарантированная доставка логов**: Logstash обеспечивает возможность настройки механизмов доставки логов с гарантированным сохранением в Elasticsearch, например, с использованием очередей сообщений.

- **Обеспечение поиска и фильтрации по записям логов**: Elasticsearch обладает мощным механизмом поиска и фильтрации данных, что позволяет разработчикам эффективно анализировать логи.

- **Пользовательский интерфейс с доступом для разработчиков**: Kibana предоставляет пользовательский интерфейс для анализа данных, включая поиск по записям логов. Разработчики могут получить доступ к Kibana для анализа и поиска логов.

- **Возможность сохранения поисков**: Kibana также предоставляет возможность сохранения поисковых запросов, что позволяет пользователям создавать ссылки на сохранённые поисковые запросы и делиться ими с другими пользователями.

Выбор Elastic Stack обоснован его широкими возможностями в области сбора, хранения и анализа логов, а также простотой использования и настройки для микросервисной архитектуры.


## Задача 3: Мониторинг

Предложите решение для обеспечения сбора и анализа состояния хостов и сервисов в микросервисной архитектуре.
Решение может состоять из одного или нескольких программных продуктов и должно описывать способы и принципы их взаимодействия.

Решение должно соответствовать следующим требованиям:
- сбор метрик со всех хостов, обслуживающих систему;
- сбор метрик состояния ресурсов хостов: CPU, RAM, HDD, Network;
- сбор метрик потребляемых ресурсов для каждого сервиса: CPU, RAM, HDD, Network;
- сбор метрик, специфичных для каждого сервиса;
- пользовательский интерфейс с возможностью делать запросы и агрегировать информацию;
- пользовательский интерфейс с возможностью настраивать различные панели для отслеживания состояния системы.

Обоснуйте свой выбор.

## Ответ:
### Решение для мониторинга состояния хостов и сервисов в микросервисной архитектуре

Для обеспечения сбора и анализа состояния хостов и сервисов в микросервисной архитектуре предлагается использовать связку Grafana и Prometheus.

**Prometheus**:
   - **Сбор метрик со всех хостов**: Prometheus может собирать метрики от различных хостов через экспортеры, которые предоставляют доступ к метрикам из различных системных ресурсов.
   - **Сбор метрик состояния ресурсов хостов**: Prometheus может собирать метрики о состоянии ресурсов хостов, таких как CPU, RAM, HDD, Network, используя соответствующие экспортеры.
   - **Сбор метрик потребляемых ресурсов для каждого сервиса**: Prometheus позволяет интегрироваться с приложениями и собирать метрики о потреблении ресурсов каждого сервиса, используя библиотеки экспортеров или прямую интеграцию с приложениями.

**Grafana**:
   - **Пользовательский интерфейс с возможностью делать запросы и агрегировать информацию**: Grafana предоставляет гибкий и настраиваемый пользовательский интерфейс для визуализации и анализа данных, с возможностью создания запросов и агрегирования информации по метрикам.
   - **Настройка различных панелей для отслеживания состояния системы**: Grafana позволяет пользователям создавать настраиваемые панели для отслеживания состояния системы, включая метрики хостов и сервисов, с возможностью просмотра и анализа данных в реальном времени.
